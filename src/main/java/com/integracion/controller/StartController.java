package com.integracion.controller;

import com.integracion.repository.ContactoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import com.integracion.modelo.Contacto;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class StartController {

    @Autowired
    private ContactoRepository contactoRepository;

    @GetMapping("/contacto")
    public Flux<Contacto> getAllContactos() {
        return contactoRepository.findAll();
    }

    @PostMapping("/contacto")
    public Mono<Contacto> crearContacto(@RequestBody Contacto contacto) {
        // TODO: process POST request
        return contactoRepository.save(contacto);
    }

    @GetMapping(value = "/contacto/{id}")
    public Mono<ResponseEntity<Contacto>> getContacto(@PathVariable(value = "id") String id) {

        // TODO: process PUT request
        return contactoRepository.findById(id).map(savedContact -> ResponseEntity.ok(savedContact))
                .defaultIfEmpty(ResponseEntity.notFound().build());

    }

    @PutMapping(value = "/contacto/{id}")
    public Mono<Contacto> actualizar(@RequestBody Contacto contacto, @PathVariable(value = "id") String id) {

        // TODO: process PUT request

        return contactoRepository.findById(id).map(p -> {
            p.setNombre(contacto.getNombre());
            p.setApellido(contacto.getApellido());
            p.setCorreo(contacto.getCorreo());
            p.setTelefono(contacto.getTelefono());
            return p;
        }).flatMap(p -> contactoRepository.save(p));

    }

    @DeleteMapping(value = "/contacto/{id}")
    public Mono<ResponseEntity<Void>> eliminarContacto(@PathVariable(value = "id") String id) {

        return contactoRepository.findById(id)
                .flatMap(exitingContact -> contactoRepository.delete(exitingContact)
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
