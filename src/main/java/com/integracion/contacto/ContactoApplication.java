package com.integracion.contacto;

import com.integracion.repository.ContactoRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@EntityScan("com.integracion.modelo")
@ComponentScan(basePackages = "com.integracion")
@EnableReactiveMongoRepositories(basePackageClasses = ContactoRepository.class)

@SpringBootApplication
public class ContactoApplication {
	public static void main(String[] args) {
		SpringApplication.run(ContactoApplication.class, args);
	}

}
