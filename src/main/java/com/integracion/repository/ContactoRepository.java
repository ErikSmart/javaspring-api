package com.integracion.repository;

import com.integracion.modelo.Contacto;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * ContactoRepository
 */
@Repository
public interface ContactoRepository extends ReactiveMongoRepository<Contacto, String> {

}